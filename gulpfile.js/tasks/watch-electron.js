/**
 * @file:	  watch任务
 * @author:	花夏(liubiao@itoxs.com)
 * @version:   V0.0.1
 * @date:	  2017-08-08 17:27:20
 */

var gulp = require('gulp');
var path = require('path');

var electron = require('electron-connect').server.create();

gulp.task('watch:electron', function() {
    electron.start();
    gulp.watch([path.join(process.cwd(), './*.js')], electron.restart);
    gulp.watch([path.join(process.cwd(), './*.{html,js,css}'), path.join(process.cwd(), 'app/**/*.{html,js,css}')], electron.reload);
});