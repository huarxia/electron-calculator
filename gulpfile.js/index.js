/**
 * @file:	  任务入口
 * @author:	花夏(liubiao@itoxs.com)
 * @version:   V0.0.1
 * @date:	  2017-08-08 17:23:21
 */

var requireDir = require('require-dir');

requireDir('./tasks', {recurse: true});